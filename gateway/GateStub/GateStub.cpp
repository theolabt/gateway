//
// Created by tlabrosse on july 2022.
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include <iostream>
#include "GateStub.h"

using json = nlohmann::json;

namespace gateway {
//private:
	int GateStub::fillFromJson(const std::string& jsonLine) {

		auto data = json::parse(jsonLine);

		auto outputFile = data["OutputFile"];
		auto execFile = data["ExecFile"];
		auto dataFiles = data["DataFiles"];

		this->output = new OutputFile(outputFile["path"], outputFile["name"]);
		this->execFile = new ExecFile(execFile["path"], execFile["name"], this->output, execFile["cmd"],
									  execFile["cmdAlt"]);

		return 0;
	}

//public:
	GateStub::GateStub(const std::string &jsonLine) {
		fillFromJson(jsonLine);
		this->jsonLine = jsonLine;
		output->initialize();
	}

	ExecFile *GateStub::getExecFile() const {
		return execFile;
	}
	OutputFile *GateStub::getOutput() const {
		return output;
	}
	const std::string &GateStub::getJsonLine() const {
		return jsonLine;
	}
} // gateway