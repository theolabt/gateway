//
// Created by tlabrosse on july 2022.
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include <stdio.h>
#include "ExecFile.h"

#ifdef _WIN32
#define OS 1
#endif

#ifdef linux
#define OS 2
#endif

namespace gateway {
	ExecFile::ExecFile(const std::string &path, const std::string &name, OutputFile *output, const std::string &cmd)
			: File(path, name), output(output), cmd(cmd) {}

	ExecFile::ExecFile(const std::string &path, const std::string &name, OutputFile *output, const std::string &cmd,
		   const std::string &cmdLinux)
			: File(path, name), output(output), cmd(cmd), cmdLinux(cmdLinux) {}

	std::string ExecFile::run(const std::string& jsonLine) const {

		std::string cmd;
		if(OS == 1 || this->cmdLinux.empty())
			cmd = this->cmd;
		else
			cmd = this->cmdLinux;

		std::string command = cmd + " " + this->getPath() + this->getName() + " " + jsonLine;
		FILE *file = popen(command.c_str(), "r");
		char buffer[100];
		std::string stringBuff;

		if (file == nullptr) perror ("Error opening file");
		else {
			while ( !feof(file) ) {
				if ( fgets (buffer , 100 , file) == nullptr ) break;
				stringBuff += buffer;
			}
			fclose (file);
		}
		return stringBuff;
	}
} // gateway