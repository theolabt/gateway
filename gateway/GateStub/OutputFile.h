//
// Created by tlabrosse on july 2022.
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef GATEWAY_OUTPUTFILE_H
#define GATEWAY_OUTPUTFILE_H

#include "File.h"

namespace gateway {

class OutputFile: public File {
private:
	std::string read();

public:
	OutputFile(const std::string &path, const std::string &name);

	int initialize() const;

};

} // gateway

#endif //GATEWAY_OUTPUTFILE_H
