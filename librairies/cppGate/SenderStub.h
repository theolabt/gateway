//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef CPPGATE_SENDERSTUB_H
#define CPPGATE_SENDERSTUB_H

#include "Stub.h"
#include "ExecFile.h"

class SenderStub: public Stub {
private:
    ExecFile* execFile;

public:
    explicit SenderStub(ExecFile* execFile = nullptr, OutputFile* outputFile = nullptr);

    void run(std::string gatePath, std::string gateName);

    ExecFile *getExecFile() const;

    void setExecFile(ExecFile *execFile);

    std::string getSentLine() const;

    std::string serialize() const;
    std::string displayExecFile() const;
    std::string displayAll() const override;
};


#endif //CPPGATE_SENDERSTUB_H
