// //
// // Created by tlabrosse on july 2022
// // licence : GNU lgpl
// // you can contact me at : theo.labt@gmail.com
// //

// #include <iostream>
// #include "SenderStub.h"

// using namespace std;

// exemple d'utilisation en tant que sender

// int main() {

//     ExecFile* execFile = new ExecFile("/home/tlabrosse/PycharmProjects/pythonGate/", "testReceiver.py", "python3 ");
//     OutputFile* outputFile = new OutputFile("/home/tlabrosse/Bureau/gateway/c++/cppGate/build/", "outputs.json");
//     SenderStub* sndStub = new SenderStub(execFile, outputFile);

//     Dictionary* dico1 = new Dictionary("dico1");
//     dico1->addParameter("hey", "toi");
//     dico1->addParameter("para2", "value2");

//     Dictionary* dico2 = new Dictionary("dico2");
//     dico2->addParameter("para3", "value3");
//     dico2->addArgument(dico1);

//     Dictionary* dico3 = new Dictionary("dico3");
//     dico3->addParameter("heyo", "salut");

//     sndStub->addDictionary(dico2);
//     sndStub->addDictionary(dico3);

//     sndStub->run("/home/tlabrosse/Bureau/gateway/c++/gateway/build/", "gate.o");
//     outputFile->displayContent();

//     return 0;
// }
