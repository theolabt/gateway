//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef CPPGATE_FILE_H
#define CPPGATE_FILE_H

#include <iostream>
#include <string>
#include <map>

class File {
protected:
    std::string name;
    std::string path;
    bool actif = true;

public:
    File(std::string path, std::string name);

    void setInactif() { this->actif = false; }

    virtual std::string serialize() const = 0;

    virtual void display() const = 0;
};


#endif //CPPGATE_FILE_H
