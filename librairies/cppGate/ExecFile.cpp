//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include "ExecFile.h"

ExecFile::ExecFile(std::string path, std::string name, std::string cmd, std::string cmdAlt) : File(path, name), cmd(cmd), cmdAlt(cmdAlt) {}

std::string ExecFile::serialize() const {
    if(this->actif)
        return  R"({"ExecFile": {"path": ")" + this->path +
                R"(", "name": ")" + this->name +
                R"(", "cmd": ")" + this->cmd +
                R"(", "cmdAlt": ")" + this->cmdAlt +
                R"("}})";
    return "";
}

void ExecFile::display() const {
    std::cout << this->serialize() << std::endl;
}


